import numpy as np
from scipy import ndimage
from utils import np_to_image_to_disk


def obtain_sinogram(phantom: np.ndarray):
    rotation_steps = range(0, 180)
    sinogram = np.empty((len(phantom), len(rotation_steps)))
    for i, angle in enumerate(rotation_steps):
        # rotate phantom by some angle
        rotated = ndimage.rotate(phantom, angle, reshape=False, prefilter=False)
        pixel_sum = np.sum(rotated, axis=0)
        sinogram[:, i] = pixel_sum

    return sinogram
