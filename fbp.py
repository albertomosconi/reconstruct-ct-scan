import numpy as np
from numpy.fft import fft, fftshift, ifft
from scipy import ndimage
from utils import np_to_image_to_disk


def filter_sinogram(sinogram: np.ndarray):
    image_size, num_projections = sinogram.shape
    # create filter
    filt = np.abs(np.linspace(-1, 1, image_size))
    filt = fftshift(filt)
    filt = np.repeat(filt[:, None], num_projections, axis=1)
    # move sinogram to frequency domain
    fourier_sino = fft(sinogram, axis=0)
    # apply frequency filter
    filtered_sino = fourier_sino * filt
    # move sinogram back to spatial domain
    inverse_filtered_sino = np.real(ifft(filtered_sino, axis=0))

    return inverse_filtered_sino


def back_projection(sinogram: np.ndarray):
    image_size, num_projections = sinogram.shape
    # apply filter to sinogram
    sinogram = filter_sinogram(sinogram)
    # back projection
    base = np.zeros((image_size, image_size))
    rotation_steps = range(0, 180)
    for i, angle in enumerate(rotation_steps):
        projection = sinogram[:, i][None, :]
        expanded_projection = np.repeat(projection, image_size, axis=0)
        base += ndimage.rotate(
            expanded_projection, -angle, reshape=False, prefilter=False
        ).astype(np.float64)
    return base
