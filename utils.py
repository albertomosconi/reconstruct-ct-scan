import matplotlib.pyplot as plt
import numpy as np


def np_to_disk(image, filename):
    np.save(filename, image)


def np_to_image_to_disk(image, filename):
    rows, cols = image.shape
    plt.figure(figsize=(cols + 1, rows + 1), dpi=1)
    plt.imshow(image, cmap="gray", interpolation="none")
    plt.axis("off")
    plt.tight_layout()
    plt.savefig(filename, bbox_inches="tight", pad_inches=0, dpi=1)
    plt.close()
