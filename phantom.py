import numpy as np
from utils import np_to_image_to_disk

densities = [64, 128, 192, 255]


def generate_phantom(image_size: int = 256, seed=None):
    g = np.random.default_rng(seed=seed)
    # black background
    base = np.zeros((image_size, image_size))
    Y, X = np.ogrid[:image_size, :image_size]
    # define center and distance from center
    cx = cy = image_size // 2
    distance_from_center = np.sqrt((X - cx) ** 2 + (Y - cy) ** 2)
    # draw outer circle
    radius_outer = g.integers(image_size // 4, image_size // 2)
    density_outer = g.choice(densities)
    base = np.where(distance_from_center <= radius_outer, density_outer, base)
    # draw inner circle
    radius_inner = g.integers(image_size // 8, radius_outer)
    density_inner = g.choice(densities)
    base = np.where(distance_from_center <= radius_inner, density_inner, base)
    # draw offset circle
    offset_x = g.integers(-(radius_outer // 2), radius_outer // 2)
    offset_y = g.integers(-(radius_outer // 2), radius_outer // 2)
    radius_offset = g.integers(image_size // 32, radius_outer // 2)
    density_offset = g.choice(densities)
    distance_from_offset_center = np.sqrt(
        (X - cx - offset_x) ** 2 + (Y - cy - offset_y) ** 2
    )
    base = np.where(distance_from_offset_center <= radius_offset, density_offset, base)

    return base
