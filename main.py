from phantom import generate_phantom
from sinogram import obtain_sinogram
from fbp import back_projection
from utils import np_to_image_to_disk, np_to_disk
from pathlib import Path


def gen(name: str, out_dir: Path, image_size: int = 512, seed: int | None = None):
    phantom = generate_phantom(image_size, seed)
    sinogram = obtain_sinogram(phantom)
    np_to_disk(sinogram, out_dir.joinpath(f"{name}_x.npy"))
    np_to_image_to_disk(sinogram, out_dir.joinpath(f"{name}_x.jpg"))
    bp = back_projection(sinogram)
    np_to_disk(bp, out_dir.joinpath(f"{name}_y.npy"))
    np_to_image_to_disk(bp, out_dir.joinpath(f"{name}_y.jpg"))


if __name__ == "__main__":
    out_dir = Path("out")
    out_dir.mkdir(parents=True, exist_ok=True)

    for i in range(50, 100):
        print(f"generating: {i}...", end="\r")
        gen(f"{i}", out_dir)
    print("generating... DONE             ")
